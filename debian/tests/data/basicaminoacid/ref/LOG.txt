********************************************************************************
INDELible V1.03 by Will Fletcher : Simulation began at: Fri Jun 12 01:59:44 2020
********************************************************************************

  Actual Insertion : Deletion Ratio  1.0 : 0.0	(Total indel rate = 1)
  Actual Indel : Substitution Ratio  0.0833333 : 0.916667	(Total event rate = 1)
  Actual average insertion length    1
  Actual average deletion length     -nan
  Number of insertion events         1
  Number of deletion events          0
  Number of substitution events      11

  * Block 1 was completed in 0.000353 seconds.

********************************************************************************

  * Simulation completed. Whole batch took: 0.000831 seconds.

INDELible V1.03 Simulations completed at: Fri Jun 12 01:59:44 2020

********************************************************************************


 Original Control File 

-----------------------


/////////////////////////////////////////////////////////////////////////////////////
//                                                                                 //
//  INDELible V1.03 control file - basicaminoacid.txt                              //
//                                                                                 //
//      A basic introduction to the structure of the INDELible control file.       //
//                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////

// It is useful to know that anything on a line after two forward slashes is ignored.

/*
   Another useful thing to know is that anything after a forward slash and star
   is ignored until INDELible sees a star followed by a forward slash later on.
*/     

[TYPE] AMINOACID 2	//  EVERY control file must begin with a [TYPE] command.
			//  The number after "AMINOACID" can be 1 or 2 and chooses the 
			//  algorithm that INDELible uses (see manuscript). Both give 
			//  identical results but in some cases one is quicker.
			//  Other blocks and commands following this statement
			//  can come in any order you like.

[SETTINGS]
  [randomseed] 1568746 // seed for random generator so outputs can be checked

[MODEL]    modelname         //  Evolutionary models are defined in [MODEL] blocks.
  [submodel] WAG             //  Here the substitution model is simply set as WAG.
  [indelmodel]  POW  1.7 500 //  Power law insertion/deletion length distribution (a=1.7)
  [indelrate]   0.1          //  insertion rate = deletion rate = 0.1
                             //  relative to average substitution rate of 1.   

[TREE] treename  (A:0.1,B:0.1);        //  User trees are defined here

[PARTITIONS] partitionname             //  [PARTITIONS] blocks say which models go with
  [treename modelname 20]            //  which trees and define the length of the
                                       //  sequence generated at the root (20 here).

[EVOLVE] partitionname 5 outputname  //  This will generate 5 replicate datasets 
                                       //  from the [PARTITIONS] block named above.

// The true alignment will be output in a file named outputname_TRUE.phy
// The unaligned sequences will be output in a file named outputname.fas
// To learn how to implement more complicated simulations (or different 
// models) please consult the manual or the other example control files.
