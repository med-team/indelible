indelible (1.03-6) unstable; urgency=medium

  * d/control: add myself to uploaders.
  * d/control: declare compliance to standards version 4.6.2.
  * d/s/lintian-overrides: flag false positive error.

 -- Étienne Mollier <emollier@debian.org>  Sat, 30 Dec 2023 19:11:32 +0100

indelible (1.03-5) unstable; urgency=medium

  * Team upload.

  [ Andreas Tille ]
  * Fix edam syntax input+output is singular
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

  [ Pranav Ballaney ]
  * Add test data
  * Add autopkgtests

 -- Pranav Ballaney <ballaneypranav@gmail.com>  Fri, 12 Jun 2020 00:52:56 +0530

indelible (1.03-4) unstable; urgency=medium

  * Team upload.
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Thu, 11 Oct 2018 20:18:57 +0200

indelible (1.03-3) unstable; urgency=medium

  [ Andreas Tille ]
  * Team upload.
  * Normalise Vcs fields using cme fix dpkg-control
  * Add fake watch file
  * debhelper 10
  * Fix some spelling issues

  [ Steffen Moeller ]
  * Added reference to OMICtools

  [ Fabian Klötzl ]
  * remove unnecessary whitespace from the end of sequences
  * bump standards version (minor changes)
  * enable hardening

 -- Andreas Tille <tille@debian.org>  Tue, 03 Oct 2017 08:19:39 +0200

indelible (1.03-2) unstable; urgency=medium

  * Added man page
  * Improved patches
  * Added EDAM file

 -- Fabian Klötzl <kloetzl@evolbio.mpg.de>  Sun, 07 Feb 2016 16:30:22 +0100

indelible (1.03-1) unstable; urgency=medium

  * Initial release. (Closes: #807486)
  * Use Autotools for the build
  * Added index.html for the documentation
  * Added missing #includes

 -- Fabian Klötzl <kloetzl@evolbio.mpg.de>  Wed, 09 Dec 2015 14:42:52 +0100
